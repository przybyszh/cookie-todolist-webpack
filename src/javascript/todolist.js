import Cookies from 'js-cookie';

const $input = document.querySelector('.main__task-input');
const $listTitle = document.querySelector('.main__list-title');
const $taskItem = document.querySelector('.main__list-item');
const $taskItemWrapper = document.querySelector('.main__list-items');
const $buttonAdd = document.querySelector('.main__task-button');
const cookieExpires = 365;

class ToDoList {

  constructor() {
    this.checkCookies();
    this.readFromCookies();
    this.enterEvent();
    this.removeTask();
  }

  readFromCookies() {
    if (Cookies.get('tasks')) {
      return this.taskFromCookies();
    }
  }

  enterEvent() {
    $input.addEventListener('keydown', e => {
      (e.keyCode === 13) ? $buttonAdd.click() : null
    });
  }

  checkCookies() {
    if (Cookies.get('tasks')) {
      const dataTasks = Cookies.getJSON('tasks');
      this.getTaskValue(dataTasks);
    } else {
      Cookies.set('tasks', {}, { expires: cookieExpires });
      const dataTasks = Cookies.getJSON('tasks');
      this.getTaskValue(dataTasks);
    }
  }

  getTaskValue(dataTasks) {
    $buttonAdd.addEventListener('click', (e) => {
      if ($input.value === '') {
        $input.classList.add('error');
      } else {
        $input.classList.remove('error');
        const i = document.querySelectorAll('.main__list-item').length
        dataTasks[`task${i}`] = $input.value
        this.setTask(`task${i}`, $input.value)
        this.setTaskCookie(dataTasks);
        $input.value = '';
      }
    })
  }

  setTaskCookie(newDataTask) {
    Cookies.set('tasks', newDataTask, { expires: cookieExpires });
  }

  setTask(key, value) {
    const checkSVG = `
      <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="45.701px" height="45.7px" fill="#FFFFFF" viewBox="0 0 45.701 45.7" style="enable-background:new 0 0 45.701 45.7;" xml:space="preserve">
      <g>
        <g>
          <path d="M20.687,38.332c-2.072,2.072-5.434,2.072-7.505,0L1.554,26.704c-2.072-2.071-2.072-5.433,0-7.504
            c2.071-2.072,5.433-2.072,7.505,0l6.928,6.927c0.523,0.522,1.372,0.522,1.896,0L36.642,7.368c2.071-2.072,5.433-2.072,7.505,0
            c0.995,0.995,1.554,2.345,1.554,3.752c0,1.407-0.559,2.757-1.554,3.752L20.687,38.332z"/>
        </g>
      </g>
      </svg>`;

    $taskItemWrapper.insertAdjacentHTML('afterbegin',
      // <button class="btn__done" type="button" data-tooltip="done">${checkSVG}</button>
      `<li class="main__list-item" data-key="${key}">
        <span>${checkSVG} ${value} </span>
        <button class="btn__remove" type="button" data-tooltip="remove">-</button>
      </li >`
    );
    this.removeTask();
    // this.doneTask();
  }

  removeTask() {
    const $buttonRemove = document.querySelectorAll('.btn__remove');
    for (const button of $buttonRemove) {
      button.addEventListener('click', (e) => {
        e.currentTarget.parentNode.remove();
        const key = e.target.parentNode.getAttribute('data-key');
        const objCookies = Cookies.getJSON('tasks');
        delete objCookies[`${key}`];
        Cookies.set('tasks', objCookies, { expires: cookieExpires });

        // let doneArray = JSON.parse(Cookies.get('tasks-done'));
        // const newDoneArray = doneArray.filter(e => e !== `${key}`)
        // Cookies.set('tasks-done', JSON.stringify(newDoneArray));
      });
    }
  }

  taskFromCookies() {
    const objCookies = Cookies.getJSON('tasks');
    for (const key of Object.keys(objCookies)) {
      this.setTask(key, objCookies[key]);
    }
  }

  doneTask() {
    let doneArray = [];
    const $buttonDoneAll = document.querySelectorAll('.btn__done');
    for (const $buttonDone of $buttonDoneAll) {
      $buttonDone.addEventListener('click', (e) => {
        e.currentTarget.parentNode.classList.add('is-done');
        doneArray.push(e.currentTarget.parentNode.getAttribute('data-key'));
        Cookies.set('tasks-done', JSON.stringify(doneArray));
      });
    }
  }
}


export default new ToDoList($input, $listTitle, $taskItem, $taskItemWrapper, $buttonAdd, cookieExpires);